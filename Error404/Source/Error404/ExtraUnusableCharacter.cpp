// Fill out your copyright notice in the Description page of Project Settings.


#include "ExtraUnusableCharacter.h"

// Sets default values
AExtraUnusableCharacter::AExtraUnusableCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AExtraUnusableCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AExtraUnusableCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AExtraUnusableCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

