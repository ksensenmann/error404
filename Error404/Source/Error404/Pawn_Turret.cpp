// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn_Turret.h"
#include "Kismet/GameplayStatics.h"

void APawn_Turret::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(FireRateTimerHandle, this, &APawn_Turret::CheckFireCondition, FireRate, true, false);
	
	PlayerPawn = Cast<APawn_Tank>(UGameplayStatics::GetPlayerPawn(this, 0));
}

// Called every frame
void APawn_Turret::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!PlayerPawn || ReturnDistanceToPlayer() > 1000)
	{
		return;
	}
	RotateTurret(PlayerPawn->GetActorLocation());

}

void APawn_Turret::CheckFireCondition()
{
	if (!PlayerPawn || !PlayerPawn->GetIsPlayerAlive())
	{
		return;
	}
	if (ReturnDistanceToPlayer() <= 1000)
	{
		Fire();
	}
}

float APawn_Turret::ReturnDistanceToPlayer()
{
	if (!PlayerPawn)
	{
		return 0;
	}
	float Distance = (GetActorLocation() - PlayerPawn->GetActorLocation()).Size();
	return Distance;
}

void APawn_Turret::HandleDestruction()
{
	Super::HandleDestruction();
	Destroy();
}
