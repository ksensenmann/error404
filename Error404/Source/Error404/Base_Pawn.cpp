// Fill out your copyright notice in the Description page of Project Settings.


#include "Base_Pawn.h"
#include "Kismet/KismetMathLibrary.h"
#include "Error404/BaseProjectile.h"
#include "Kismet/GameplayStatics.h"
#include "Error404/HealthComponent.h"
// Sets default values
ABase_Pawn::ABase_Pawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Collider"));
	RootComponent = CapsuleComp;
	BodyComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body"));
	BodyComp->SetupAttachment(RootComponent);
	BodyComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BarrelComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Turret"));
	BarrelComp->SetupAttachment(RootComponent);
	SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("Scene colissa"));
	SceneComp->SetupAttachment(BarrelComp);
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
}

void ABase_Pawn::PawnDestroyed()
{
	HandleDestruction();
}

// Called when the game starts or when spawned
void ABase_Pawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABase_Pawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABase_Pawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}
void ABase_Pawn::RotateTurret(FVector LookAtTarget)
{
	FVector StartLocation = BarrelComp->GetComponentLocation();
	FRotator TurretRotation = UKismetMathLibrary::FindLookAtRotation(StartLocation, FVector(LookAtTarget.X, LookAtTarget.Y, BarrelComp->GetComponentLocation().Z));

	BarrelComp->SetWorldRotation(TurretRotation);
	//float abc = TurretRotation.Yaw;
	//FRotator StartRotationLocal = BarrelComp()->GetSocketRotation("Socket Name");
	//UE_LOG(LogTemp, Warning, TEXT("%s"), &QuatRotation.ToString);
	//if(TurretRotation >= 0)
}

void ABase_Pawn::Fire()
{
	
	if (ProjectileClass)
	{
		FRotator jaloley = SceneComp->GetComponentRotation();
		FVector heyloley = SceneComp->GetComponentLocation();

		ABaseProjectile* TempProjectile = GetWorld()->SpawnActor < ABaseProjectile>(ProjectileClass, heyloley, jaloley);
		TempProjectile->SetOwner(this);
	}
}

void ABase_Pawn::HandleDestruction()
{
	UGameplayStatics::SpawnEmitterAtLocation(this, DeathParticle, GetActorLocation());
	UGameplayStatics::PlaySoundAtLocation(this, DeathSound, GetActorLocation());
	GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(DeathShake, 1);

}