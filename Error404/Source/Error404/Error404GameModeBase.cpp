// Fill out your copyright notice in the Description page of Project Settings.


#include "Error404GameModeBase.h"
#include "Error404/Pawn_Tank.h"
#include "Error404/Pawn_Turret.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerControllerBase.h"

void AError404GameModeBase::BeginPlay()
{
	// Get references and game win/lose conditions.
	TSubclassOf<APawn_Tank> ClassToFind;
	ClassToFind = APawn_Turret::StaticClass();
	TArray<AActor*> TurretActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassToFind, TurretActors);
	TargetTurrets = TurretActors.Num();
	PlayerTank = Cast < APawn_Tank>(UGameplayStatics::GetPlayerPawn(this, 0));

	PlayerControllerRef = Cast<APlayerControllerBase>(UGameplayStatics::GetPlayerController(this, 0));
	HandleGameStart();
	// Call HandleGameStart to initialise the start countdown, turret activation, pawn check etc.
}

void AError404GameModeBase::ActorDied(AActor* DeadActor)
{
	if (DeadActor == PlayerTank)
	{
		PlayerTank->PawnDestroyed();
		HandleGameOver(false);

		if (PlayerControllerRef)
		{
			PlayerControllerRef->SetPlayerEnabledState(false);
		}
	}
	else if (APawn_Turret* DestroyedTurret = Cast<APawn_Turret>(DeadActor))
	{
		DestroyedTurret->PawnDestroyed();
		//HandleGameOver(false);
		TargetTurrets--;
		if (TargetTurrets == 0)
		{
			HandleGameOver(true);
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("An Actor died"));

	// Check what type of Actor died. If Turret, tally. If Player -> go to lose condition.
}


void AError404GameModeBase::HandleGameStart()
{
	GameStart();
	if (PlayerControllerRef)
	{
		PlayerControllerRef->SetPlayerEnabledState(false);
		FTimerHandle PlayerEnableHandle;
		FTimerDelegate PlayerEnableDelegate = FTimerDelegate::CreateUObject(PlayerControllerRef, &APlayerControllerBase::SetPlayerEnabledState, true);
		GetWorldTimerManager().SetTimer(PlayerEnableHandle, PlayerEnableDelegate, StartDelay, false);
	}



	// initialise the start countdown, turret activation, pawn check etc.
	// Call Blueprint version GameStart();

}

void AError404GameModeBase::HandleGameOver(bool PlayerWon)
{
	GameOver(PlayerWon);
	// See if the player has destroyed all the turrets, show win result.
	// else if turret destroyed player, show lose result. 
	// Call blueprint version GameOver();
}
