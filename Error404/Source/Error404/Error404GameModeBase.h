// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Error404GameModeBase.generated.h"

class APawn_Turret;
class APawn_Tank;
class APlayerControllerBase;
UCLASS()
class ERROR404_API AError404GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

public:
	void ActorDied(AActor* DeadActor);
	UFUNCTION(BlueprintImplementableEvent)
		void GameStart();
	UFUNCTION(BlueprintImplementableEvent)
		void GameOver(bool PlayerWon);

private:

	APlayerControllerBase* PlayerControllerRef;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Loop", meta = (AllowPrivateAccess = "true"))
	int32 StartDelay = 4;
	int32 TargetTurrets = 0;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player", meta = (AllowPrivateAccess = "true"))
		APawn_Tank* PlayerTank;
		void HandleGameStart();
	void HandleGameOver(bool PlayerWon);

};