// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Base_Pawn.h"
#include "Pawn_Tank.h"
#include "Pawn_Turret.generated.h"

/**
 * 
 */
UCLASS()
class ERROR404_API APawn_Turret : public ABase_Pawn
{
	GENERATED_BODY()
protected:

	virtual void BeginPlay() override;
	virtual void HandleDestruction() override;
public:
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	float FireRate =1.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	float Health = 1.0f;

	void CheckFireCondition();
	float ReturnDistanceToPlayer();
	APawn_Tank* PlayerPawn;
	FTimerHandle FireRateTimerHandle;

};