// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Base_Pawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Pawn_Tank.generated.h"

class USpringArmComponent;
class UCameraComponent;

/**
 *
 */
UCLASS()
class ERROR404_API APawn_Tank : public ABase_Pawn
{
	GENERATED_BODY()
public:
	APawn_Tank();
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
		UCameraComponent* CameraComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
		USpringArmComponent* SpringArmComp;
	
protected:
	virtual void BeginPlay() override;
	virtual void HandleDestruction() override;

public:
	virtual void Tick(float DeltaTime) override;
virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

bool GetIsPlayerAlive();

private:
	bool bIsPlayerAlive = true;
	APlayerController* PlayerControllerRef;
	FHitResult TraceHitResult;

	float MoveSpeed = 300.0f;
	float RotateSpeed = 100.0f;
	FVector MoveDirection;
	FQuat RotationDirection;

	void CalculateMoveInput(float Value);
	void CalculateRotateInput(float Value);
	
	void Move();
	void Rotate();

};
